package com.qa.test;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pages.ForgotPassword;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;

public class LoginPageTest extends TestBase{
	
	LoginPage loginPage;
	HomePage homePage;
	ForgotPassword forgotPassword;
	
	public LoginPageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() {
		initialization();
		loginPage = new LoginPage();
	}
	/*
	 * @Test//(enabled=false) public void testLoginPageTitle() { String
	 * titleOfLoginPage = loginPage.validateTitle();
	 * //Assert.assertEquals(titleOfLoginPage, "AHC - Login"); }
	 */
	
	/*
	 * @Test//(enabled=false) public void testWelcomeImage() { boolean welcomeImage
	 * = loginPage.validateWelcomeImage(); //Assert.assertTrue(welcomeImage); }
	 * 
	 * @Test//(enabled=false) public void testAccessLogo() { boolean accessLogo =
	 * loginPage.validateAccessLogo(); //Assert.assertTrue(accessLogo); }
	 */
	
	@Test//(enabled=false)
	public void testLogin() throws Exception {
		homePage = loginPage.validateLogin(prop.getProperty("un"), prop.getProperty("pwd"));
		//Assert.assertEquals(driver.getTitle(),"UM SEARCH MEMBER | AHC");
	}
	
	/*
	 * @Test public void testForgotPassword() { forgotPassword =
	 * loginPage.validateForgotPassword(); //Assert.assertEquals(driver.getTitle(),
	 * "Forgot Password |"); }
	 */
	
	@AfterMethod
	public void tearDown() throws InterruptedException {
		Thread.sleep(3000);
	//	driver.close();
	}

}
