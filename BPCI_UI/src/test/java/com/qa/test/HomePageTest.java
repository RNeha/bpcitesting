package com.qa.test;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.util.DBConnect;

public class HomePageTest extends TestBase{
	LoginPage loginPage;
	HomePage homePage;
	String excelPath="";
	ArrayList<HashMap<String, String>> queryResultList;
	//ResultSet rs = DBConnect.getResultSetObj();	
	public HomePageTest() {
		super();
	}
	
	
	@BeforeMethod
	public void setUp() throws Exception {
		initialization();
		loginPage = new LoginPage();
		homePage = new HomePage();
		loginPage.validateLogin(prop.getProperty("un"), prop.getProperty("pwd"));
		
	}
 
  
	@Test
	public void getOverAllEpisodeCount() throws Exception {
		try {
	//	excelPath = prop.getProperty("excel");
		String text= homePage.validateOverAllCount();
		System.out.println("OverAllEpisodeCount is"+text);
		
		String query = "declare @fromdate as datetime,@todate as datetime\r\n" + 
				"set @fromdate=(SELECT DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0))\r\n" + 
				"set @todate=(SELECT DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0)))\r\n" + 
				"select      \r\n" + 
				"     distinct count( episode.episodeID) as overAllCount\r\n" + 
				"   from tbl_episode episode    \r\n" + 
				"  where   \r\n" + 
				"      (episode.observation_End_Date between @fromdate and @todate)  \r\n" + 
				"   or (episode.startdate between @fromdate and @todate)  \r\n" + 
				"   or (episode.startDate<=@fromdate and episode.observation_End_Date>=@todate)  \r\n" + 
				"   or (episode.startDate<=@fromdate and episode.observation_End_Date is NULL)  \r\n" + 
				"   or (@fromdate between episode.startDate and episode.observation_End_Date)    \r\n" + 
				"         or (@todate between   episode.startDate and episode.observation_End_Date)";
		//DBConnect.connectDB(query);
		
		DBConnect.connectDB(query);
			/*
			 * while (rs.next()) { String result=rs.getString(1);
			 * System.out.println("queryResult"+result); }
			 */
		
	
		Object uiResult=homePage.validateOverAllCount();
		DBConnect.getResultSetObj();
		//Object queryRes=rs.getString("overAllCount");
		//Assert.assertEquals(uiResult,queryRes );
		System.out.println("pass");
	  
	}catch (Exception e) {
		e.printStackTrace();
	} 
	finally {
		DBConnect.closeConnection();
	}
		
		
		
}
	 

		
	
	@AfterMethod
	public void tearDown() throws InterruptedException {
		Thread.sleep(3000);
		driver.close();
	}
}
