package com.qa.pages;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.google.common.io.Files;
import com.qa.base.TestBase;

public class LoginPage extends TestBase {
	
	// Object Repository
	/*
	 * @FindBy(xpath="(//div[@id='WelcomeImage']/child::node())[2]")
	 * 
	 * @CacheLookup WebElement welcomeImage;
	 * 
	 * @FindBy(xpath=
	 * "//img[@src='/LoginResources/Image/access_health_care_logo_final-01.png']")
	 * 
	 * @CacheLookup WebElement accessLogo;
	 */
	@FindBy(id="normal_login_EMail")
	@CacheLookup
	WebElement username;
	
	@FindBy(id="normal_login_Password")
	@CacheLookup
	WebElement password;
	
	@FindBy(xpath  = "//*[@class='ant-btn login-form-button ant-btn-primary']")
	@CacheLookup
	WebElement submitButton;
	
	/*
	 * @FindBy(xpath="//a[text()='Forgot Your Password?']")
	 * 
	 * @CacheLookup WebElement forgotPassword;
	 */
	
	//Initialization of all WebElement
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	/*
	 * //Validations public String validateTitle() { return driver.getTitle(); }
	 * 
	 * public boolean validateWelcomeImage() { return welcomeImage.isDisplayed(); }
	 * 
	 * public boolean validateAccessLogo() { return accessLogo.isDisplayed(); }
	 */
	
	public HomePage validateLogin(String un, String pwd) throws Exception {
		username.sendKeys(un);
		password.sendKeys(pwd);
		submitButton.click();
		File src = submitButton.getScreenshotAs(OutputType.FILE);
		Files.copy(src, new File(""));
		Thread.sleep(3000);
		
		return new HomePage();
	}
	
	/*
	 * public ForgotPassword validateForgotPassword() { 
	 * forgotPassword.click();
	 * 
	 * return new ForgotPassword(); }
	 */
}
