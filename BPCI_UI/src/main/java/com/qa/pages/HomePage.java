package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.TestBase;

public class HomePage extends TestBase{
	@FindBy(xpath = "//div[@class='Active-card-Background']//*[@class='gutter-box active-card-text card-number-font-size ml-4']")
	
	WebElement overAllEpisodeCount;
	
	@FindBy(xpath = "(//div[@class='gutter-box inactive-card-text card-number-font-size ml-4'])[1]")
	
	WebElement activeEpisodeCount;
	
	@FindBy(xpath = "(//div[@class='gutter-box inactive-card-text card-number-font-size ml-4'])[2]")
	
	WebElement observationEpisodeCount;
	
	@FindBy(xpath = "(//div[@class='gutter-box inactive-card-text card-number-font-size ml-4'])[3]")
	
	WebElement closedEpisodeCount;
	
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public String validateOverAllCount() {
		overAllEpisodeCount.click();
		String text = overAllEpisodeCount.getText();
		System.out.println("UIcount"+text);
		return text;
	}
	
	
	
	

}
